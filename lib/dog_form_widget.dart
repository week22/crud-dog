import 'package:dog_app/dog.dart';
import 'package:dog_app/dog_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DogForm extends StatefulWidget {
  Dog dog;
  DogForm({Key? key, required this.dog}) : super(key: key);

  @override
  _DogFormState createState() => _DogFormState(dog);
}

class _DogFormState extends State<DogForm> {
  final _formkey = GlobalKey<FormState>();
  Dog dog;
  _DogFormState(this.dog);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Dog Form')),
      body: Form(
        key: _formkey,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              TextFormField(
                autofocus: true,
                initialValue: dog.name,
                decoration: InputDecoration(labelText: 'Name'),
                onChanged: (String? value) {
                  dog.name = value!;
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input name';
                  }
                  return null;
                },
              ),
              TextFormField(
                initialValue: dog.age.toString(),
                decoration: InputDecoration(labelText: 'Age'),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                ],
                onChanged: (String? value) {
                  dog.age = int.parse(value!);
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input Age';
                  }
                  return null;
                },
              ),
              ElevatedButton(
                  onPressed: () async {
                    if (_formkey.currentState!.validate()) {
                      if (dog.id > 0) {
                        await saveDog(dog);
                      } else {
                        await addNewDog(dog);
                      }
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save'))
            ],
          ),
        ),
      ),
    );
  }
}
